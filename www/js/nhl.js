const API_URL = 'https://wms.nhllogistics.vn/api';
const DB_VERSON = '1.0';
const TIMEOUT = 60 * 60 * 1000;
var NHL = (function () {
    var NHL = function () {
        var that = this;
        var loadedDB = false;
        this.data = {
            api_url: API_URL,
            api_token: null,
            username: null,
            password: null,
            firstname: 'Guest',
            lastname: null,
            mobile: null,
            last_login: null,
        }
        let db = openDatabase('nhl', DB_VERSON, 'Nguyen Ha Logistics', 2 * 1024 * 1024);
        this.db = db;
        db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS ACCOUNT (username, password, last_login, token, home, firstname, lastname, mobile)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS SETTING (id, vl)');
            tx.executeSql('SELECT vl from SETTING where id = ?', ['api_url'], function (tx, results) {
                if (!(results && results.rows && results.rows.length)) {
                    tx.executeSql('INSERT INTO SETTING(id,vl) VALUES(?,?)', [
                        'api_url'
                        , API_URL
                    ], function (tx, result) {
                        //console.log(err); 
                    }, function (tx, err) {
                        console.log(err);
                    });
                } else {
                    that.data.api_url = results.rows[0].vl || API_URL;
                }
            }, function (tx, err) {
                console.log(err);
            });
            tx.executeSql('SELECT username, password, token as api_token, home, firstname, lastname, mobile, last_login from ACCOUNT ORDER BY last_login desc LIMIT 1 ', [], function (tx, results) {
                if (results && results.rows && results.rows.length) {
                    data = results.rows[0] || {};
                    that.data = Object.assign(that.data, data);
                    if (!data.last_login) {
                        that.data.api_token = null;
                    } else {
                        var dura = data.last_login;
                        if (typeof dura == "string") {
                            dura = new Date(dura);
                        }
                        let target = dura.getTime() + TIMEOUT - new Date().getTime();
                        if (target<1000 * 60) {
                            that.data.api_token = null;
                        }else{
                            setTimeout(() => {
                                nhl.api_login();
                            }, target);
                        }
                    }
                }
                loadedDB = true;
                app.emit('loadedDB')
            }, function (tx, err) {
                console.log(err);
                loadedDB = true;
                app.emit('loadedDB')
            });
        });
        function waitToken(cb) {
            if (that.data.api_token && that.data.api_token.length) {
                cb && cb();
            } else {
                setTimeout(() => {
                    waitToken(cb);
                }, 100);
            }
        }
        NHL.prototype.sql = function (sql, param, callback) {
            this.db.transaction(function (tx) {
                if (typeof param === "function") {
                    callback = param;
                }
                if (!param || !Array.isArray(param)) {
                    param = [];
                }
                tx.executeSql(sql, param,
                    function (tx, results) {
                        if (results && results.rows && results.rows.length) {
                            if (callback) callback(results.rows);
                        } else {
                            callback && callback(null)
                        }
                    }, function (tx, err) {
                        console.log(err);
                        if (callback) callback(null);
                    }
                );
            });
        }
        NHL.prototype.saveAccount = function (username, password, token, home, cb) {
            this.db.transaction(function (tx) {
                function insert() {
                    tx.executeSql('INSERT INTO ACCOUNT(username,password, last_login, token, home) VALUES(?,?,?,?,?)', [
                        username,
                        password,
                        new Date().toJSON(),
                        token,
                        home
                    ], function (tx, results) {
                        if (cb) {
                            cb(true);
                        }
                    }, function (tx, err) {
                        console.log(err)
                        if (cb) {
                            cb(false);
                        }
                    });
                }
                function update() {
                    tx.executeSql(`UPDATE ACCOUNT
                        SET  password = ?, last_login = ?, token = ?, home = ? 
                        WHERE username = ?`, [
                        password,
                        new Date().toJSON(),
                        token,
                        home,
                        username
                    ], function (tx, results) {
                        if (cb) {
                            cb(true);
                        }
                    }, function (tx, err) {
                        console.log(err)
                        if (cb) {
                            cb(false);
                        }
                    });
                }
                tx.executeSql('SELECT username AS dem FROM ACCOUNT WHERE username = ?', [username], function (tx, results) {
                    if (!(results && results.rows && results.rows.length)) {
                        insert();
                    } else {
                        update();
                    }
                }, function (tx, err) {
                    insert();
                })
            })
        };
        NHL.prototype.api_login = function (u, p, cb) {
            app.request.promise.post(`${that.data.api_url}/user/login`, JSON.stringify({
                username: u, password: p
            }), 'json').then(res => {
                let data = res.data;
                if (data.Code == 0) {
                    that.data.api_token = data.Token;
                    that.data.username = u;
                    that.data.password = p;
                    that.data.last_login = new Date();
                    setTimeout(() => {
                        nhl.api_login();
                    }, TIMEOUT);
                    that.saveAccount(u, p, data.Token, data.Home, function () {
                        cb && cb();
                    })
                    app.emit("changedToken", data.Token);
                } else {
                    cb && cb(data.Msg || 'Error!')
                }
            }).catch(error => {
                cb && cb(error)
            });

        }
        NHL.prototype.api_update_profile = function (firstname, lastname, mobile, cb) {
            waitToken(() => {
                app.request.promise({
                    url: `${that.data.api_url}/user/profile`,
                    data: { firstname, lastname, mobile },
                    method: 'PUT',
                    dataType: 'json',
                    contentType: 'application/json',
                    headers: {
                        'Authorization': 'Bearer ' + nhl.data.api_token
                    }
                }).then(res => {
                    let data = res.data;
                    if (data.Code == 0) {
                        that.data = Object.assign(that.data, {
                            firstname,
                            lastname,
                            mobile,
                        });
                        app.emit("changedProfile");
                        that.sql(`UPDATE ACCOUNT SET firstname = ?, lastname = ?, mobile = ? where username = ?`, [
                            firstname,
                            lastname,
                            mobile,
                            that.data.username
                        ], cb);
                    }
                }).catch(error => {
                    cb && cb(error)
                });
            });
        };

        NHL.prototype.clearAccount = function (cb) {
            that.sql(`UPDATE ACCOUNT SET password = NULL, token = null`, function (err) {
                app.emit("changedPass");
                cb && cb(errerr);
            });
        }

        NHL.prototype.api_change_pass = function (old_pass, new_pass, cb) {
            waitToken(() => {
                app.request.promise({
                    url: `${that.data.api_url}/user`,
                    data: { oldpass: old_pass, password: new_pass },
                    method: 'PUT',
                    dataType: 'json',
                    contentType: 'application/json',
                    headers: {
                        'Authorization': 'Bearer ' + nhl.data.api_token
                    }
                }).then(res => {
                    let data = res.data;
                    if (data.Code == 0) {
                        that.data = Object.assign(that.data, {
                            password: new_pass,
                            api_token: null
                        });
                        app.emit("loadedDB");
                        that.sql(`UPDATE ACCOUNT SET password = ? where username = ?`, [
                            new_pass,
                            that.data.username
                        ], cb);
                    }
                }).catch(error => {
                    cb && cb(error)
                });
            });
        }
        NHL.prototype.isLogged = function (cb) {
            loadedDB && cb && cb(this.data.api_token && this.data.api_token.length);
        }
        // NHL.prototype.api_getInput = function (cb) { 
        //     app.request.promise({
        //         url: `${that.data.api_url}/ops/import/putaway`,
        //         //data: { oldpass: old_pass, password: new_pass },
        //         method: 'GET',
        //         dataType: 'json',
        //         contentType: 'application/json',
        //         headers: {
        //             'Authorization': 'Bearer ' + nhl.data.api_token
        //         }
        //     }) .then(res => {
        //         let data = res.data;
        //         if (data.Code == 0) {
        //             cb && cb(data.Data || []);
        //         } else {
        //             cb && cb(data.Msg || 'Error!')
        //         }
        //     }).catch(error => {
        //         cb && cb(error)
        //     });
        // }
        NHL.prototype.api_getInput = function (cb) {
            waitToken(() => {
                app.request.promise.get(`${that.data.api_url}/ops/import/putaway`, null, 'json').then(res => {
                    let data = res.data;
                    if (data.Code == 0) {
                        cb && cb(data.Data || []);
                    } else {
                        cb && cb(data.Msg || 'Error!')
                    }
                }).catch(error => {
                    cb && cb(error)
                });
            });
        }
        NHL.prototype.api_getInputDetail = function (id,cb) {
            waitToken(() => {
                app.request.promise.get(`${that.data.api_url}/ops/import/putaway/detail?w_import_id=${id}`, null, 'json').then(res => {
                    let data = res.data;
                    if (data.Code == 0) {
                        cb && cb(data.Data || []);
                    } else {
                        cb && cb(data.Msg || 'Error!')
                    }
                }).catch(error => {
                    cb && cb(error)
                });
            });
        }
        NHL.prototype.api_getInputedSku = function (id, sku,cb) {
            waitToken(() => {
                app.request.promise.get(`${that.data.api_url}/ops/import/putaway/sku?w_import_id=${id}&w_sku=${sku}`, null, 'json').then(res => {
                    let data = res.data;
                    if (data.Code == 0) {
                        cb && cb(data.Data || []);
                    } else {
                        cb && cb(data.Msg || 'Error!')
                    }
                }).catch(error => {
                    cb && cb(error)
                });
            });
        }
    }
    return NHL;
})();
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
    module.exports = NHL;
else
    window.NHL = NHL;