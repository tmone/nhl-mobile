var $$ = Dom7;
var nhl = new NHL();
var INPUT_TMP = {};
var app = new Framework7({
  root: '#app', // App root element

  id: 'vn.nguyenhalogistic.nhl', // App bundle ID
  name: 'nhl', // App name
  theme: 'auto', // Automatic theme detection

  nhl: function () {
    return nhl
  },

  // App root data
  data: function () {
    return {

    };
  },
  // App root methods
  methods: {
    setInfo: function () {
      var a = (nhl.data.lastname || '') + ' ' + (nhl.data.firstname || '');
      if (a.length < 2) {
        a = nhl.data.username;
      }

      $$(".title-name").text(a);
      //$$(".user-name").text("(" + nhl.data.username + ")");
    },
    setToken: function () {
      var header = {
        "Content-Type": "application/json"
      }
      if (nhl.data && nhl.data.api_token) {
        header['Authorization'] = `Bearer ${nhl.data.api_token}`
      }
      app.request.setup({
        headers: header
      });
    },
    changePass: function () {
      app.dialog.create({
        title: "Đổi mật khẩu",
        text: "",
        content: `
        <form class="change-password-form">
          <div class="dialog-input-field dialog-input-double input">
            <input type="password" name="old_password" placeholder="Mật khẩu cũ..." class="old-password">
          </div>
          <div class="dialog-input-field dialog-input-double input">
            <input type="password" name="new_password" placeholder="Mật khẩu mới..." class="new-password">
          </div>
          <div class="dialog-input-field dialog-input-double input">
            <input type="password" name="ver_password" placeholder="Xác nhận Mật khẩu mới..." class="ver-password">
          </div>
        </form>
        `,
        destroyOnClose: true,
        buttons: [
          {
            keyCodes: [16, 32, 45],
            text: 'Hủy',
            cssClass: "left",
            onClick: function (d, e) {

            }
          },
          {
            keyCodes: [9, 13],
            text: "Xác nhận",
            cssClass: "right",
            onClick: function (d, e) {
              var pa = app.form.convertToData('.change-password-form');
              if (pa.old_password != nhl.data.password) {
                app.toast.create({
                  text: 'Mật khẩu cũ không chính xác!',
                  closeButton: true,
                  closeTimeout: 2000,
                }).open();
                return;
              }
              if (pa.new_password != pa.ver_password) {
                app.toast.create({
                  text: 'Mật khẩu xác nhận không giống!',
                  closeButton: true,
                  closeTimeout: 2000,
                }).open();
                return;
              }
              if (pa.new_password.length < 8) {
                app.toast.create({
                  text: 'Mật khẩu mới quá ngắn (Ít nhất 8 ký tự)!',
                  closeButton: true,
                  closeTimeout: 2000,
                }).open();
                return;
              }
              app.dialog.close();
              nhl.api_change_pass(pa.old_password, pa.new_password, function (err) {
                if (err) {
                  app.toast.create({
                    text: 'Ồ Lỗi rồi. Thử lại nhé!',
                    closeButton: true,
                    closeTimeout: 2000,
                  }).open();
                  return;
                }
                app.toast.create({
                  text: 'Đổi mật khẩu thành công. Đăng nhập lại!',
                  closeButton: true,
                  closeTimeout: 2000,
                  on: {
                    close: function () {
                      nhl.clearAccount();
                    },
                  }
                }).open();
              })
            },
            bold: true
          },
        ]
      }).open();
      $$(".change-password-form .old-password").focus();
    }
  },
  // App routes
  routes: routes,


  // Input settings
  input: {
    scrollIntoViewOnFocus: Framework7.device.cordova && !Framework7.device.electron,
    scrollIntoViewCentered: Framework7.device.cordova && !Framework7.device.electron,
  },
  // Cordova Statusbar settings
  statusbar: {
    iosOverlaysWebView: true,
    androidOverlaysWebView: false,
  },
  on: {
    init: function () {
      var f7 = this;
      if (f7.device.cordova) {
        // Init cordova APIs (see cordova-app.js)
        cordovaApp.init(f7);
      }
    },
    loadedDB: function () {
      var f7 = this;
      nhl.isLogged(function (vl) {
        if (!vl) {
          f7.loginScreen.open('#nhl-login-screen');
          if (nhl.data.username && nhl.data.username.length) {
            f7.form.fillFromData('#nhl-login-screen', {
              username: nhl.data.username,
              password: ''
            });
            $$('#nhl-login-screen .password').focus()
          } else {
            $$('#nhl-login-screen .username').focus()
          }
        } else {
          f7.loginScreen.close('#nhl-login-screen');
          app.methods.setInfo();
        }
      });
      app.methods.setInfo();
      app.methods.setToken()
    },
    changedProfile: function (profile) {
      app.methods.setInfo();
    },
    changedToken: function (token) {
      app.methods.setToken()
    },
    changedPass: function () {
      app.loginScreen.open('#nhl-login-screen');
      if (nhl.data.username && nhl.data.username.length) {
        app.form.fillFromData('#nhl-login-screen', {
          username: nhl.data.username,
          password: ''
        });
        $$('#nhl-login-screen .password').focus()
      } else {
        $$('#nhl-login-screen .username').focus()
      }
    }
  },
});
$$(document).on('deviceready', function () {

})
// Login Screen Demo
function login() {
  var formData = app.form.convertToData('#nhl-login-screen');
  if (formData && formData.username && formData.username.length &&
    formData.password && formData.password.length) {
    nhl.api_login(formData.username, formData.password, function (err) {
      if (!err) {
        app.loginScreen.close();
      } else {
        app.toast.create({
          text: 'Tên đăng nhập hoặc Mật khẩu không chính xác',
          closeButton: true,
          closeTimeout: 2000,
        }).open();
      }
    })
    // nhl.saveAccount(formData, function () {
    //   app.loginScreen.close('#my-login-screen');
    // })
  } else {
    app.toast.create({
      text: 'Hãy nhập đủ tên đăng nhập và mật khẩu',
      closeButton: true,
      closeTimeout: 2000,
    }).open();
  }
}
$$('#nhl-login-screen .password').on('keydown', function (e) {
  if (e.which == 9 || e.which == 13) {
    login();
  }
});
$$('#nhl-login-screen .login-button').on('click', function () {
  login()

});
$$('.logout-button').on('click', function () {
  nhl.clearAccount();
});
$$(".change-pass").on('click', function () {
  app.methods.changePass();
});